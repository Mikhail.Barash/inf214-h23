# INF214 - Extra Obligatory (for those who failed Oblig1/Oblig2/both)
# !!! Deadline: Friday 10.11.2023 at 14:00 !!!
# !!! Deadline CANNOT be extended !!!

| Task                        | Points |
|----------------------------|--------|
| Task 1          | __10__      |
| Task 2          | __15__      |
| Task 3          | __15__      |
| Total                      | __40__     |
| Required points to pass    | personalized for every student     |


## How to submit the assignment?

Submit solutions as PDF, TXT, DOC, or DOCX files.
The name of the file should contain your name and UiB user handle, e.g., `MikhailBarash_yez013.pdf`.

Task 3 is a MittUiB quiz, please see details below.

## Task 1 

Consider the following program written in the "AWAIT" language.

```
int x = 0;
int y = 0;
int z = 0;

sem lock1 = 1;
sem lock2 = 1;

process foo {
   z = z + 2;
   P(lock1);
   x = x + 2;
   P(lock2);
   V(lock1);
   y = y + 2;
   V(lock2);
}

process bar {
   P(lock2);
   y = y + 1;
   P(lock1);
   x = x + 1;
   V(lock1);
   V(lock2);
   z = z + 1;
}    
```

* **1.1. Can this program deadlock? If yes, then how? If no, then why not?**
* **1.2. What are the possible final values of `x`, `y`, and `z` in the deadlock state?**
* **1.3. What are the possible final values of `x`, `y`, and `z` if the program terminates?**

## Task 2

Explain the code at this link:

https://plnkr.co/edit/Q1jyGXvy9INMRG3Y?preview


* **2.1. What does `then` do in function `go` and how does `then` work?**
* **2.2. Why do we have `return new Promise(...)` in function `showCircle`?**
* **2.3. What does `return new Promise(...)` do?**
* **2.4. Why do we have `setTimeout` in line 46?**
* **2.5. What is the meaning of lines 50–53?**
* **2.6. What is the meaning of `0` in line 54, and what consequences does it bring?**

## Task 3

**READ CAREFULLY BEFORE CLICKING THE LINK**

- This is a quiz with "true"/"false" questions, done via MittUiB.
- The quiz has 15 questions.
- Each correct answer gives 1 point.
- Points are not deducted for wrong answers.
- **The time limit for doing Task 3 via MittUiB is 30 minutes.**

Link to the MittUiB quiz: https://mitt.uib.no/courses/42541/quizzes/37636



