import java.util.concurrent.*;

// class `Tuple` represents a tuple

class Tuple<T1, T2> {
    private final T1 first;
    private final T2 second;

    public Tuple(T1 first, T2 second) {
        this.first = first;
        this.second = second;
    }

    public T1 getFirst() {
        return first;
    }

    public T2 getSecond() {
        return second;
    }
}


public class Main {
    
    public static <T1, T2> Tuple<T1, T2> run_both(Callable<T1> f1, Callable<T2> f2) throws InterruptedException, ExecutionException {

        // TODO: read about `Callable`

        // Hint: look into `ExecutorService`, `Future`, and thread pools
        
        /* TODO: ... code here ... */

        try {
            // Run the two functions `f1` and `f2`
            Future<T1> run1 = /* TODO: ... code here ... */;
            Future<T2> run2 = /* TODO: ... code here ... */;

            // Wait for the both runs to complete
            T1 result1 = run1.get();
            T2 result2 = run2.get();

            return new Tuple<>(result1, result2);
            
        } finally {
            /* TODO: ...*/
        }
    }


    public static void main(String[] args) {

        // example of using the function `run_both`:

        // f1:
        Callable<Integer> f1 = () -> {
            Thread.sleep(1000);
            return 42;
        };

        // f2:
        Callable<String> f2 = () -> "Greetings INF214!";

        try {
            Tuple<Integer, String> res = run_both(f1, f2);
            System.out.println("Result of function1: " + res.getFirst());
            System.out.println("Result of function2: " + res.getSecond());
        }
        catch (InterruptedException | ExecutionException e) { }
    }
}


