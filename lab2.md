# INF214 H23 - Lab 2 - for week 37

## Presentation by a group leader

* Reasoning about concurrent programs (based on slides here: https://mitt.uib.no/courses/42541/files?preview=5293724)

## Tasks to solve before the group session and to discuss during the group session

* **The tasks are unrelated to each other, unless otherwise noted.**

### Task 1

Consider the following program:

```java
/*LINE 1*/ int a = 0, b = 0, c = 0;

/*LINE 2*/ a = 1;
/*LINE 3*/ b = b + a;
/*LINE 4*/ c = a + b * a;

/*LINE 5*/ if (a>0) {
/*LINE 6*/    a = 42;
/*LINE 7*/ }
```

* Recall the notions of _state_, _history_, _property_, _true property_ from here: https://mitt.uib.no/courses/42541/files/folder/lecture-slides/29-aug-2023?preview=5279420
* For example, the state of the program after line 1 is can be written down as: `(a=0, b=0, c=0)`, and after line 2 as: `(a=1, b=0, c=0)`, etc.
* Write down a history of this program. You can use the arrow symbol (`->`) to designate a step in the execution: for example, partial history up to line 3 can be written as: `(a=0, b=0, c=0) -> (a=1, b=0, c=0)`.
* How many possible histories can this program have?
* Is the Boolean predicate `P(a) = (a>10)` a true property?

### Task 2

Assume we have the following shared variables:

```java
int x, y, z;
```


* Split the following program into blocks of code that appear atomic:

```java
x = 0;
y = y + 1;
int a = x + y;
a = a * 100;
a = sin(a);
z = a;
```

* Is your subdivision unique?


### Task 3

* Define class `Account` with integer field `money`, among other fields/methods that you might need/want.

* Implement method

```java
public void send(Account a, Account b, int amount)
```


that allows money transfers between specific accounts.

* Make sure that no deadlock can happen.

* **Hint:** you may want to rewatch MittUiB pre-recorded videos 4 and 6 about the _Dining Philosophers Problem_.



## Reading documentation

* Read about fairness of `ReentrantLock`s in Java: https://docs.oracle.com/cd/E17802_01/j2se/j2se/1.5.0/jcp/beta1/apidiffs/java/util/concurrent/locks/ReentrantLock.html
* Make sure you can explain what the fairness parameter of the class `ReentrantLock` constructor is, how it can be used, and why.

## Early preparation for the exam

* Make a small cheat sheet about scheduling policies (_unconditional fairness_, _weak fairness_, _strong fairness_).
Don't just copy-paste the definitions from the slides / the book / the videos, but rather make small examples that you can use yourself to understand / quickly recall the difference between these three types of fairness.
