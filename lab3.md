# INF214 H23 - Lab 3 - for week 38

# Part I: Presentation by a group leader

* Semaphores in Java.

# Part II: Tasks to solve before the group session and to discuss during the group session

* **The tasks are unrelated to each other, unless otherwise noted.**

## Task 1

Consider the following program:
```java
int x = VALUE1;
int y = VALUE2;
x = x + y;
y = x - y;
x = x - y;
```
Add pre- and post-conditions before and after each statement to characterize the effects of this program.

In particular, what are the final values of `x` and `y`?

## Task 2


Consider the following program:
```
int x = 10;
bool c = true;

co
   <await (x==0)>;
   c = false;
||
   while (c) <x = x-1;>
oc
```

* Will the program terminate if scheduling is weakly fair? Explain.
* Will the program terminate if scheduling is strongly fair? Explain.


## Task 3
* Create two threads in Java, each of which will increment the value of a shared variable `counter` 10000 times.
* __Use `ExecutorService`, `AtomicInteger`, and `Runnable`.__

## Task 4
* In Java, implement a function that takes an integer number `N`, and creates `N` threads, such that each thread with an odd number prints `"Hello"`, and each thread with an even number prints `"world"`.
* __Use `ExecutorService`.__

## Task 5

* In Java, create two threads, such that the first one returns current date, and the second one returns current time. Then, after both threads have finished their execution, print the output where the current time comes first and then the current date. Also, inform the user about which of the threads has finished first.
* __Use `ExecutorService`, `Future`.__

_Hints:_

* `import java.text.SimpleDateFormat;`

* `import java.util.Date;`

* to get current date, use `new Date()` _(this expression is of type `Date`)_

* to get current time, use `new SimpleDateFormat("HH:mm:ss").format(new Date())` _(this expression is of type `String`)_

# Part III: Reading documentation

* Read about semaphores in Java: https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Semaphore.html and make sure you understand the difference between the methods `acquire`, `acquireUninterruptibly`, and `tryAcquire`.


# Part IV: Early preparation for the exam

* Make a very small cheat sheet about the approaches to solve the Critical Section Problem that we studied. 
