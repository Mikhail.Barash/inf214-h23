# INF214 H23 - Lab 4 - for week 40

# Tasks to solve before the group session and to discuss during the group session

* **The tasks are unrelated to each other, unless otherwise noted.**

## Task 1

Implement the solution for the Bear and Bees Problem using semaphores in Java.

The user of your program should be able to specify the values of `H` (the capacity of the pot), and `N` (the number of bees).

_Hint: You may want to look into `ExecutorService` and thread pools._

## Task 2

Implement the solution to the Producers and Consumers Problem (slide 4 of https://mitt.uib.no/courses/42541/files/folder/lecture-slides?preview=5336333, where the size of `buf` is 1) using Java semaphores.

The user of your program should be able to specify the values of `M` (how many producers are there) and `N` (how many consumers are there).

You **do not** have to implement "producing" of the values; you can rather, for example, generate random numbers.


## Task 3
Implement the solution to the Readers and Writers Problem (slide 11 of https://mitt.uib.no/courses/42541/files/folder/lecture-slides?preview=5356937) using monitors in Java.

The user of your program should be able to specify how many readers and writers are there.

_Hint: use `ReentrantLock` and `Condition` to implement condition variables (see also: https://replit.com/@mikbar/ParallelPunyEditor#Main.java)._

_Hint: you can use `Runnable`s to implement "reading task"s and "writing task"s._

You **do not** have to implement the reading/writing to the database, nor the database itself. Thus, "reading the database" can be just `System.out.println("I am reading the database");` and similarly for "writing to the database".


# Reading documentation

* Read about condition variables in Java: https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/locks/Condition.html. 


# Early preparation for the exam

* Try to _really_ understand the solution of the Readers and Writers Problem that used semaphores (slides 8, 9, 10 of https://mitt.uib.no/courses/42541/files/folder/lecture-slides?preview=5354600).

