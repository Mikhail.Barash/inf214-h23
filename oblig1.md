# INF214 - Obligatory 1 (deadline: Thursday 5.10.2023)

| Task                        | Points |
|----------------------------|--------|
| Task 1 :blue_book:         | __3__      |
| Task 2 :ship:              | __7__      |
| Task 3 :mountain_cableway: | __5__      |
| Task 4 :bird:              | __15__     |
| Task 5 :passport_control:  | __20__     |
| Total                      | __50__     |
| Required points to pass    | __20__ :tada:     |

## Important :exclamation:

__In this obligatory assignment, you are asked to only use the "AWAIT" language.__

__You can find a cheat sheet on the language at:__ https://mitt.uib.no/courses/42541/files?preview=5292283

## How to submit the assignment?

Submit solutions in a file (_allowed file formats:_ `DOC` - _Microsoft Word_ , `DOCX` - _Microsoft Word_, `TXT` - _Simple Text File_, `PDF` - _Portable Document Format_, `RTF` - _Rich Text Format_, `MD` - _Markdown_) at:

https://mitt.uib.no/courses/42541/assignments/79760 

The name of the file should contain your first name, surname and your UiB user handle, e.g., `MikhailBarash_yez013.pdf`.

Please also mention your name and surname inside the actual file with the solution, too.


## Task 1 (At-Most-Once Property) :blue_book:


Consider the following program:
```java
int x = 1;
int y = 1;
co
   < x = x + y; >
||
   y = 0;
||
   x = x - y;
oc
```

Does the program meet the requirements of the At-Most-Once Property?

:pencil2: __Explain your answer.__

## Task 2 (`<await ...>` statements) :ship:

Consider a collection of producers, each producing `N` items, and a collection of consumers, each consuming `N` items.
There are an equal number of producers and consumers.

The intent is that each produced item is consumed by exactly one consumer, but it is unspecified by which.

Below is an incorrect implementation of this scenario, written in the AWAIT language.

In the code:

* the variable `buf` is a one-element buffer
* the variable `buf_full` should indicate when a producer can write to `buf` (when `buf_full == false`) and when a consumer can read from `buf` (when `buf_full == true`).
 

__Your task is to add `<await ...>` statements to correct the code (and modify the code in other ways if necessary; in particular, you may consider whether the if statements will be needed). Only enclose code in an atomic block if it is necessary for correctness.__

```java
// global variables
int buf;
bool buf_full = false;
const int N = 100, Ps = 10, Cs = 10; 

 

process Producer[i = 1 to Ps] {
  int a[N]; 
  // ... here we would have some code to fill array a with data (we don't care how exactly --
  // -- you don't have to provide this code here!)
  int p = 0;
 
  while (p < N) {
    if (buf_full == true) continue;
    buf = a[p];
    buf_full = true;
    p = p + 1;
  }
}

 

process Consumer[i = 1 to Cs] {
  int b[N]; 
  int c = 0;
  
  while (c < N) {
    if (buf_full == false) continue;
    b[c] = buf;
    buf_full = false;
    c = c + 1;      
  }

  // ... here we would have some code code that uses b (we don't care how exactly -- 
  // -- you don't have to provide this code here!)
}
```

## Task 3 (Semaphores) :mountain_cableway:

Consider the following program:
```
process P1 {
    write("We");
    write("study");
}

process P2 {
    write("concurrent");
    write("programming");
}

process P3 {
    write("in");
    write("INF214");
}
```

__Your task is to add semaphores to the program so that the six lines of output are printed in the order__
```
We
study
concurrent
programming
in
INF214
```

:point_right: __Declare and initialize any semaphores you need and add `P` and `V` operations to the above processes.__

## Task 4 (Semaphores) :bird: 

Given are `N` baby birds and one parent bird.
The baby birds eat out of a common dish that initially contains `F` portions of food.
Each baby repeatedly eats one portion of food at a time, sleeps for a while, and then comes back to eat. When the dish becomes empty, the baby bird who empties the dish awakens the parent bird. The parent refills the dish with `F` portions, then waits for the dish to become empty again. This pattern repeats forever.

__Your task is to represent the birds as processes and develop code (in the AWAIT Language) that simulates their actions.__

:point_right: __Use semaphores for synchronization.__



## Task 5 (Semaphores) :passport_control: 

Consider Multilandet, which is a country, and Multibyen, which is its capital city.
The airport of Multibyen has introduced strict entry requirements for arriving passengers because of a global pandemic.
Some of the passengers are vaccinated, while others are not vaccinated.
All passengers arriving to Multibyen must go through the document/passport control, where their vaccination certificates are checked by border guard officers.

Upon arriving to Multibyen Airport, passengers, both vaccinated and unvaccinated, are mingling in the Mingling Zone, and they are walking towards to the Documents Checking Zone.
There, the border guard checks their vaccination certificates, imposes quarantine on the unvaccinated ones, and in any case lets all the passengers into the city.

 

Assume that the passengers enter the Documents Checking Zone in a random order. The only requirement is that there must be never unvaccinated and vaccinated passengers in the Documents Checking Zone at the same time. However, people with the same vaccination status are allowed in the Documents Checking Zone at the same time (that is, at any given moment of time, either all passengers in the Documents Checking Zone are vaccinated, or all passengers in the Documents Checking Zone are unvaccinated).

 
Assume that there are `N` vaccinated passengers, and `M` unvaccinated passengers who have just landed at Multibyen Airport.

 
__Your task is to simulate the described situation in the AWAIT language.__

:point_right: __Represent passengers as processes.__

:point_right: __Use semaphores for synchronization.__

:point_right: __Make sure that your solution avoids deadlock.__

:bulb: __Your solution need NOT to be fair.__

